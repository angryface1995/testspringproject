# Spring boot RESTful API test project

Test project that implements simple CRUD operations with simple pagination and custom queries methods.
Functionality was tested via [Postman](https://www.getpostman.com/).

## Built With

* [Spring Boot](https://spring.io/projects/spring-boot)
* [Spring Data JPA](https://spring.io/projects/spring-data-jpa)
* [Maven](https://maven.apache.org/)
* [Hibernate](http://hibernate.org/orm/documentation/5.4/)
* [PostgreSQL](https://www.postgresql.org/docs/9.6/index.html)

## Database used

I used horror movies database sample kindly provided by [peskaj](https://github.com/peschkaj/).
I thought that it's rather complicated db for this project.
[Link](https://github.com/peschkaj/horror-movies-database) to this database on github.
Did some changes to movies table - created and attached sequence to primary key.

## Database schema

![schema](/dbforspring.png)