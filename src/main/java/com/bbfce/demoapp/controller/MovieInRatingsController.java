package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Movie;
import com.bbfce.demoapp.repository.MovieRepository;
import com.bbfce.demoapp.repository.RatingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieInRatingsController {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    RatingsRepository ratingsRepository;

    @GetMapping("/ratings/{ratingId}/movies")
    public List<Movie> getMoviesByRatingID(@PathVariable Long ratingId){
        if(!ratingsRepository.existsById(ratingId)){
            throw new NotFoundException("Rating not found!");
        }

        return movieRepository.findAllByRatingsId(ratingId);
    }

    @GetMapping("/ratings/sorted/{ratingId}/movies")
    public List<Movie> getAllMoviesByCountryIdOrdered(@PathVariable Long ratingId){
        if(!ratingsRepository.existsById(ratingId)){
            throw new NotFoundException("Rating not found!");
        }

        return movieRepository.findAllByRatingsIdOrderByIdAsc(ratingId);
    }

}
