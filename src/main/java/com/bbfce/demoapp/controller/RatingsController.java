package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Movie;
import com.bbfce.demoapp.model.Ratings;
import com.bbfce.demoapp.repository.RatingsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class RatingsController {

    @Autowired
    private RatingsRepository ratingsRepository;

    @GetMapping("/ratings")
    public List<Ratings> getAllRatings(){
        return ratingsRepository.findAll();
    }

    @GetMapping("/ratings/{id}")
    public Ratings getRatingByID(@PathVariable long id){
        Optional<Ratings> optRatings = ratingsRepository.findById(id);
        if(optRatings.isPresent()){
            return optRatings.get();
        }else {
            throw new NotFoundException("Rating not found with id" + id);
        }
    }

    @GetMapping("/ratings/queries/1")
    public List<Ratings> getRatingsWithSomeSort(){
        return ratingsRepository.getRatingsWithSomeSort();
    }

    @GetMapping("/paginated/ratings")
    public List<Ratings> getRatingsPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable){
        Page page = ratingsRepository.findAll(pageable);
        return page.getContent();
    }

    @PostMapping("/ratings")
    public Ratings createRating(@Valid @RequestBody Ratings rating){
        return ratingsRepository.save(rating);
    }

    @PutMapping("/ratings/{id}")
    public Ratings updateRating(@PathVariable long id, @Valid @RequestBody Ratings ratingUpdated){
        return ratingsRepository.findById(id)
                .map(rating -> {
                    rating.setRating(ratingUpdated.getRating());
                    return ratingsRepository.save(rating);
                }).orElseThrow(() -> new NotFoundException("Rating not found with id" + id));
    }

    @DeleteMapping("/ratings/{id}")
    public String deleteRating(@PathVariable long id){
        return ratingsRepository.findById(id)
                .map(rating -> {
                    for (Movie movie : rating.getMovies()){
                        movie.setRatings(null);
                    }
                    ratingsRepository.delete(rating);
                    return "Successfully deleted.";
                }).orElseThrow(() -> new NotFoundException("Rating not found with id" + id));
    }
}
