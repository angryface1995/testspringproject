package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Locations;
import com.bbfce.demoapp.repository.LocationsRepository;
import com.bbfce.demoapp.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class LocationsController {

    @Autowired
    LocationsRepository locationsRepository;

    @Autowired
    MovieRepository moviesRepository;

    @GetMapping("/locations")
    public List<Locations> getAllLocations(){
        return locationsRepository.findAll();
    }

    @GetMapping("locations/{id}")
    public Locations getLocationByID(@PathVariable long id){
        Optional<Locations> optLocation = locationsRepository.findById(id);

        if(optLocation.isPresent()){
            return optLocation.get();
        }else{
            throw new NotFoundException("Location not found with id " + id);
        }
    }

    @GetMapping("/paginated/locations")
    public List<Locations> getLocationsPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable){
        Page page = locationsRepository.findAll(pageable);
        return page.getContent();
    }

    @PostMapping("/locations")
    public Locations createLocation(@Valid @RequestBody Locations location){
        return locationsRepository.save(location);
    }

    @PutMapping("/locations/{id}")
    public Locations updateLocation(@PathVariable long id, @Valid @RequestBody Locations locationUpdated){
        return locationsRepository.findById(id)
                .map(location -> {
                    location.setCountry_name(locationUpdated.getCountry_name());
                    location.setLocation_name(locationUpdated.getLocation_name());
                    location.setMovies(locationUpdated.getMovies());
                    return locationsRepository.save(location);
                }).orElseThrow(() -> new NotFoundException("Location not found with id " + id));
    }

    @GetMapping("/locations/queries")
    public List<Locations> getCustomLocations(){
        return locationsRepository.getLocationsWithCustomConditions();
    }

    @GetMapping("/locations/queries/1")
    public List<Locations> getAnotherCustomLocations(){
        return locationsRepository.getAnotherLocationsWithCustomConditions();
    }

    @DeleteMapping("/locations/{id}")
    public String deleteLocation(@PathVariable long id){
        return locationsRepository.findById(id)
                .map(location -> {
                    locationsRepository.delete(location);
                    return "Successfully deleted!";
                }).orElseThrow(() -> new NotFoundException("Location not found with id " + id));
    }
}
