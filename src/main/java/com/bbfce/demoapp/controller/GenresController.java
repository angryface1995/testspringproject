package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Genres;
import com.bbfce.demoapp.repository.GenresRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class GenresController {

    @Autowired
    GenresRepository genresRepository;

    @GetMapping("/genres")
    public List<Genres> getAllGenres(){
        return genresRepository.findAll();
    }

    @GetMapping("/genres/{id}")
    public Genres getGenreByID(@PathVariable long id){
        Optional<Genres> optGenre = genresRepository.findById(id);
        if(optGenre.isPresent()){
            return optGenre.get();
        }else{
            throw new NotFoundException("Genre not found with id" + id);
        }
    }

    @GetMapping("/genres/queries/{genreId}")
    public List<Genres> getGenresSpecific(@PathVariable Long genreId){
        return genresRepository.getGenresWithCustomConditions(genreId);
    }

    @GetMapping("/paginated/genres")
    public List<Genres> getGenresPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC)Pageable pageable){
        Page page = genresRepository.findAll(pageable);
        return page.getContent();
    }

    @PostMapping("/genres")
    public Genres createGenre(@Valid @RequestBody Genres genre){
        return genresRepository.save(genre);
    }

    @PutMapping("/genres/{id}")
    public Genres updateGenre(@PathVariable long id, @Valid @RequestBody Genres genreUpdated){
        return genresRepository.findById(id)
                .map(genre -> {
                    genre.setGenre_name(genreUpdated.getGenre_name());
                    genre.setMovies(genreUpdated.getMovies());
                    return genresRepository.save(genre);
                }).orElseThrow(() -> new NotFoundException("Genre not found with id " + id));
    }

    @DeleteMapping("/genres/{id}")
    public String deleteGenre(@PathVariable long id){
        return genresRepository.findById(id)
                .map(genre -> {
                    genresRepository.delete(genre);
                    return "Successfully deleted!";
                }).orElseThrow(() -> new NotFoundException("Genre not found with id " + id));
    }
}
