package com.bbfce.demoapp.controller;


import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Movie;
import com.bbfce.demoapp.repository.ActorsRepository;
import com.bbfce.demoapp.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class MovieController {

    @Autowired
    MovieRepository movieRepository;

    @Autowired
    ActorsRepository actorsRepository;

    @GetMapping("/movies")
    public List<Movie> getAllMovies(){
        return movieRepository.findAll();
    }

    @GetMapping("/paginated/movies")
    public List<Movie> getMoviesPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable){
        Page page = movieRepository.findAll(pageable);
        return page.getContent();
    }

    @GetMapping("/movies/{id}")
    public Movie getMovieByID(@PathVariable long id){
        Optional<Movie> optMovie = movieRepository.findById(id);
        if(optMovie.isPresent()){
            return optMovie.get();
        }else{
            throw new NotFoundException("Movie not found with id" + id);
        }
    }

    @PutMapping("/movies/{id}")
    public Movie updateMovie(@PathVariable Long id, @Valid @RequestBody Movie movieUpdated){
            return movieRepository.findById(id)
                    .map(movie -> {
                        movie.setTitle(movieUpdated.getTitle());
                        movie.setRatings(movieUpdated.getRatings());
                        movie.setCountries(movieUpdated.getCountries());
                        movie.setReview(movieUpdated.getReview());
                        movie.setRuntime(movieUpdated.getRuntime());
                        movie.setBudget(movieUpdated.getBudget());
                        movie.setPlot(movieUpdated.getPlot());
                        return movieRepository.save(movieUpdated);
                    }).orElseThrow(() -> new NotFoundException("There is no movie with id " + id));
        }


    @PostMapping("/movies")
    public Movie createMovie(@Valid @RequestBody Movie movie){
        return movieRepository.save(movie);
    }

    @DeleteMapping("/movies/{id}")
    public String deleteMovieByID(@PathVariable long id){
        return movieRepository.findById(id)
                .map(movie -> {
                    movieRepository.delete(movie);
                    return "Successfully deleted!";
                }).orElseThrow(() -> new NotFoundException("Movie not found with id" + id));
    }
}
