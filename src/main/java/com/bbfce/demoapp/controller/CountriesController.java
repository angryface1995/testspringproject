package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Countries;
import com.bbfce.demoapp.model.Movie;
import com.bbfce.demoapp.repository.CountriesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CountriesController {

    @Autowired
    private CountriesRepository countriesRepository;

    @GetMapping("/countries")
    public List<Countries> getAllCountries(){
        return countriesRepository.findAll();
    }

    @GetMapping("/countries/{id}")
    public Countries getCountryByID(@PathVariable long id){
        Optional<Countries> optCountry = countriesRepository.findById(id);

        if(optCountry.isPresent()){
            return optCountry.get();
        }else{
            throw new NotFoundException("Country not found with id " + id);
        }
    }

    @GetMapping("/paginated/countries")
    public List<Countries> getCountriesPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC)Pageable pageable){
        Page page = countriesRepository.findAll(pageable);
        return page.getContent();
    }

    @GetMapping("/countries/queries/1")
    public List<Countries> getCountriesWithSomeSort(){
        return countriesRepository.getCountriesWithSomeSort();
    }

    @PostMapping("/countries")
    public Countries createCountry(@Valid @RequestBody Countries country){
        return countriesRepository.save(country);
    }

    @PutMapping("/countries/{id}")
    public Countries updateCountry(@PathVariable long id, @Valid @RequestBody Countries countryUpdated){
        return countriesRepository.findById(id)
                .map(country -> {
                    country.setCountry_name(countryUpdated.getCountry_name());
                    return countriesRepository.save(country);
                }).orElseThrow(() -> new NotFoundException("Country not found with id " + id));
    }

    @DeleteMapping("/countries/{id}")
    public String deleteCountry(@PathVariable long id){

        return countriesRepository.findById(id)
                .map(country -> {
                    for (Movie movie : country.getMovies()){
                        movie.setCountries(null);
                    }
                    countriesRepository.delete(country);
                    return "Successfully deleted.";
                }).orElseThrow(() -> new NotFoundException("Student not found with id " + id));
    }
}
