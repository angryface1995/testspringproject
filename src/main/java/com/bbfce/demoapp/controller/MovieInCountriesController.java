package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Movie;
import com.bbfce.demoapp.repository.CountriesRepository;
import com.bbfce.demoapp.repository.MovieRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/api")
public class MovieInCountriesController {

    @Autowired
    CountriesRepository countriesRepository;

    @Autowired
    MovieRepository moviesRepository;

    @GetMapping("/countries/{countryId}/movies")
    public List<Movie> getMoviesByCountriesID(@PathVariable Long countryId){
        if(!countriesRepository.existsById(countryId)){
            throw new NotFoundException("There is no country with id " + countryId);
        }

        return moviesRepository.findAllByCountriesId(countryId);
    }

    @GetMapping("/countries/sorted/{countryId}/movies")
    public List<Movie> getAllMoviesByCountryIdOrdered(@PathVariable Long countryId){
        if(!countriesRepository.existsById(countryId)){
            throw new NotFoundException("There is no country with id " + countryId);
        }

        return moviesRepository.findAllByCountriesIdOrderByIdAsc(countryId);
    }

}
