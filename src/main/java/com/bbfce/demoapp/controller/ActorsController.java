package com.bbfce.demoapp.controller;

import com.bbfce.demoapp.exception.NotFoundException;
import com.bbfce.demoapp.model.Actors;
import com.bbfce.demoapp.repository.ActorsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.web.PageableDefault;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/api")
public class ActorsController {

    @Autowired
    private ActorsRepository actorsRepository;

    @GetMapping("/actors")
    public List<Actors> getAllActors(){
        return actorsRepository.findAll();
    }

    @GetMapping("/actors/{id}")
    public Actors getActorByID(@PathVariable long id){
        Optional<Actors> optActor = actorsRepository.findById(id);
        if(optActor.isPresent()){
            return optActor.get();
        }else{
            throw new NotFoundException("Actor not found with id " + id);
        }
    }

    @GetMapping("/paginated/actors")
    public List<Actors> getActorsPage(@PageableDefault(page = 0, size = 5, sort = {"id"}, direction = Sort.Direction.ASC) Pageable pageable){
        Page page = actorsRepository.findAll(pageable);
        return page.getContent();
    }

    @GetMapping("/actors/queries/{movieId}")
    public List<Actors> getActorsWithoutMovies(@PathVariable Long movieId){
        return actorsRepository.getAllActorsByMovieId(movieId);
    }

   @PostMapping("/actors")
    public Actors createActor(@Valid @RequestBody Actors actor){
       return actorsRepository.save(actor);
   }

   @PutMapping("/actors/{id}")
    public Actors updateActor(@PathVariable long id, @Valid @RequestBody Actors actorUpdated){
        return actorsRepository.findById(id)
                .map(actor -> {
                    actor.setMovies(actorUpdated.getMovies());
                    actor.setActor_name(actorUpdated.getActor_name());
                    return actorsRepository.save(actor);
                }).orElseThrow(() -> new NotFoundException("Actor not found with id " + id));
   }

   @DeleteMapping("/actors/{id}")
    public String deleteStudent(@PathVariable long id){
        return actorsRepository.findById(id)
                .map(actor -> {
                    actorsRepository.delete(actor);
                    return "Successfully deleted!";
                }).orElseThrow(() -> new NotFoundException("Actor not found with id " + id));
   }
}
