package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Actors;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActorsRepository extends JpaRepository<Actors, Long> {

    @Query("SELECT actors.id, actors.actor_name FROM Actors actors JOIN actors.movies movies WHERE movies.id =:id")
    List<Actors> getAllActorsByMovieId(@Param("id") Long id);

}
