package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Movie;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface MovieRepository extends JpaRepository<Movie, Long> {

    List<Movie> findByRatingsIsNotNull();

    List<Movie> findAllByCountriesId(Long country_id);

    List<Movie> findAllByRatingsId(Long rating_id);

    List<Movie> findAllByCountriesIdOrderByIdAsc(Long country_id);

    List<Movie> findAllByRatingsIdOrderByIdAsc(Long rating_id);

}
