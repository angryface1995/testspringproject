package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Countries;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface CountriesRepository extends JpaRepository<Countries, Long> {

    @Query("SELECT c.id, c.country_name, count(m.countries.id) FROM Countries c JOIN c.movies m WHERE c.id = m.countries.id group by c.id order by count(m.countries.id) desc ")
    List<Countries> getCountriesWithSomeSort();
}
