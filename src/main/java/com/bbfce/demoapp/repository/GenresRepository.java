package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Genres;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface GenresRepository extends JpaRepository<Genres, Long> {

    @Query("SELECT g.id, g.genre_name, m.id, m.title FROM Genres g JOIN g.movies m WHERE m.release_year >= 1990 AND m.runtime IS NOT NULL AND g.id =:id")
    List<Genres> getGenresWithCustomConditions(@Param("id") Long id);
}
