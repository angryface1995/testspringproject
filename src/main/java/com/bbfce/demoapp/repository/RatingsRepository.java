package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Ratings;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface RatingsRepository extends JpaRepository<Ratings, Long> {

    @Query("SELECT r.id, r.rating, count(m.ratings.id) FROM Ratings r JOIN r.movies m WHERE r.id = m.ratings.id group by r.id order by count(m.ratings.id) desc ")
    List<Ratings> getRatingsWithSomeSort();
}
