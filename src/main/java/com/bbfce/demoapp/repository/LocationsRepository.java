package com.bbfce.demoapp.repository;

import com.bbfce.demoapp.model.Locations;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

public interface LocationsRepository extends JpaRepository<Locations, Long> {

    @Query("SELECT l.id, l.country_name, l.location_name FROM Locations l " +
            "WHERE substring(l.country_name, 1, 1)=substring(l.location_name, length(l.location_name) , 1)")
    List<Locations> getLocationsWithCustomConditions();

    @Query("SELECT l.id, l.country_name, l.location_name FROM Locations l " +
            "WHERE substring(l.country_name, 1, 1)=substring(l.location_name, 1, 1)")
    List<Locations> getAnotherLocationsWithCustomConditions();

}
