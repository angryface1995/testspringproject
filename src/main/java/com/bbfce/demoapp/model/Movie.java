package com.bbfce.demoapp.model;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import javax.persistence.*;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "movies")
@JsonIgnoreProperties({"hibernateLazyInitializer", "handler"})
public class Movie implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", nullable = false, updatable = false)
    private Long id;

    @ManyToMany(mappedBy = "movies")
    @JsonIgnoreProperties("movies")
    private Set<Actors> actors = new HashSet<>();

    @ManyToMany(mappedBy = "movies")
    @JsonIgnoreProperties("movies")
    private Set<Genres> genres = new HashSet<>();

    @ManyToMany(mappedBy = "movies")
    @JsonIgnoreProperties("movies")
    private Set<Locations> locations = new HashSet<>();

    @PreRemove
    private void removeMovieFromAGL() {
        for (Actors actor : actors) {
            actor.getMovies().remove(this);
        }
        for (Locations location : locations){
            location.getMovies().remove(this);
        }
        for (Genres genre : genres){
            genre.getMovies().remove(this);
        }
    }

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "country_id")
    @JsonIgnoreProperties("movies")
    private Countries countries;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "rating_id")
    @JsonIgnoreProperties("movies")
    private Ratings ratings;

    @Column(name = "title")
    private String title;

    @Column(name = "release_year")
    private Integer release_year;

    @Column(name = "review")
    private Double review;

    @Column(name = "runtime")
    private Integer runtime;

    @Column(name = "budget")
    private Double budget;

    @Column(name = "plot")
    private String plot;

    public Movie(){

    }

    public Movie(String title, int release_year, double review, int runtime, double budget, String plot) {
        this.title = title;
        this.release_year = release_year;
        this.review = review;
        this.runtime = runtime;
        this.budget = budget;
        this.plot = plot;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Set<Actors> getActors() {
        return actors;
    }

    public void setActors(Set<Actors> actors) {
        this.actors = actors;
    }
    public Set<Genres> getGenres() {
        return genres;
    }

    public void setGenres(Set<Genres> genres) {
        this.genres = genres;
    }
    public Set<Locations> getLocations() {
        return locations;
    }

    public void setLocations(Set<Locations> locations) {
        this.locations = locations;
    }

    public Countries getCountries() {
        return countries;
    }

    public void setCountries(Countries countries) {
        this.countries = countries;
    }

    public Ratings getRatings() {
        return ratings;
    }

    public void setRatings(Ratings ratings) {
        this.ratings = ratings;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public Integer getRelease_year() {
        return release_year;
    }

    public void setRelease_year(Integer release_year) {
        this.release_year = release_year;
    }

    public Double getReview() {
        return review;
    }

    public void setReview(Double review) {
        this.review = review;
    }

    public Integer getRuntime() {
        return runtime;
    }

    public void setRuntime(Integer runtime) {
        this.runtime = runtime;
    }

    public Double getBudget() {
        return budget;
    }

    public void setBudget(Double budget) {
        this.budget = budget;
    }

    public String getPlot() {
        return plot;
    }

    public void setPlot(String plot) {
        this.plot = plot;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Movie movie = (Movie) o;
        return id.equals(movie.id);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id);
    }

}
